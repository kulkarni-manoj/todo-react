import firebase from  "firebase";
const firebaseApp = firebase.initializeApp({
    apiKey: "AIzaSyB13VReRt10CCdrjQdtS6wCcH7P22geMMY",
    authDomain: "todo-app-rct.firebaseapp.com",
    databaseURL: "https://todo-app-rct.firebaseio.com",
    projectId: "todo-app-rct",
    storageBucket: "todo-app-rct.appspot.com",
    messagingSenderId: "44037672733",
    appId: "1:44037672733:web:f491c0cadd9d6fb5dd5bf4"
})

const db = firebaseApp.firestore();

export default db;